using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.IO.Compression;
using System.IO;

public class BuildTool : MonoBehaviour
{
    static string path = "C:/Users/riege/Desktop/Builds/Build/";
    static string[] clientScenes = { "Assets/Scenes/LoginRegister.unity", "Assets/Scenes/MenuScene.unity", "Assets/Scenes/ClientScene.unity" };
    static string[] clientOnlyScenes = { "Assets/Scenes/ClientScene.unity" };
    static string[] masterScenes = { "Assets/Scenes/MasterServer.unity" };
    static string[] serverScenes = { "Assets/Scenes/ServerScene.unity" };


    [MenuItem("Build/All")]
    public static void BuildAll()
    {
        Build("Client/", "Client.exe", clientScenes, BuildTarget.StandaloneWindows64, BuildOptions.None);
        Build("Servers/Server/", "Server.x86_64", serverScenes, BuildTarget.StandaloneLinux64, BuildOptions.EnableHeadlessMode);
        Build("Servers/MasterServer/", "MasterServer.x86_64", masterScenes, BuildTarget.StandaloneLinux64, BuildOptions.EnableHeadlessMode);
        AddDatabase();
        MakeZip();
    }

    [MenuItem("Build/Client")]
    public static void BuildClient()
    {
        Build("Client/", "Client.exe", clientScenes, BuildTarget.StandaloneWindows64, BuildOptions.None);
    }

    [MenuItem("Build/Servers Windows")]
    public static void BuildWindows()
    {
        Build("Client/", "Client.exe", clientOnlyScenes, BuildTarget.StandaloneWindows64, BuildOptions.None);
        Build("Servers/Server/", "Server.exe", serverScenes, BuildTarget.StandaloneWindows64, BuildOptions.EnableHeadlessMode);
        Build("Servers/MasterServer/", "MasterServer.exe", masterScenes, BuildTarget.StandaloneWindows64, BuildOptions.EnableHeadlessMode);
        AddDatabase();
    }


    [MenuItem("Build/Servers Linux")]
    public static void BuildLinux()
    {
        Build("Servers/Server/", "Server.x86_64", serverScenes, BuildTarget.StandaloneLinux64, BuildOptions.EnableHeadlessMode);
        Build("Servers/MasterServer/", "MasterServer.x86_64", masterScenes, BuildTarget.StandaloneLinux64, BuildOptions.EnableHeadlessMode);
        AddDatabase();
        MakeZip();
    }




    private static void Build(string directory, string fileName, string[] sceneNames, BuildTarget target, BuildOptions options)
    {
        int _len = EditorBuildSettings.scenes.Length;
        List<EditorBuildSettingsScene> _scenes = new List<EditorBuildSettingsScene>();

        foreach (var scene_name in sceneNames)
        {
            foreach (var scene in EditorBuildSettings.scenes)
            {
                if (scene_name.Equals(scene.path))
                {
                    _scenes.Add(scene);
                }
            }
        }

        string _path = path + directory + fileName;
        if (Directory.Exists(path + directory))
        {
            Directory.Delete(path + directory, true);
        }
        UnityEditor.Build.Reporting.BuildReport report = BuildPipeline.BuildPlayer(_scenes.ToArray(), _path, target, options);
        Debug.Log(report.summary.result);
    }

    public static void AddDatabase()
    {
        string dir = path + "Servers/MasterServer/MasterServer_Data/Database/";
        Directory.CreateDirectory(dir);
        File.Copy(path + "db.db", dir + "db.db");
    }

    private static void MakeZip()
    {
        if (File.Exists(path + "server.zip"))
        {
            File.Delete(path + "server.zip");
        }
        ZipFile.CreateFromDirectory(path + "Servers", path + "server.zip");
    }
}
