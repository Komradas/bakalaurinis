using Mono.Data.Sqlite;
using System.Data;
using UnityEngine;
using Database;

public class Db
{
    static IDbConnection dbcon;
    static object lockObject = new object();
    private static string connectionString
    {
        get
        {
            return "URI=file:" + Application.dataPath + "/Database/db.db";
        }
    }

    public static Player GetPlayer(string _user)
    {
        var coll = Get($"SELECT * FROM Players where username = '{_user}'");
        if (coll.Length == 4)
        {
            Player player = new Player();
            player.id = int.Parse(coll[0]);
            player.username = coll[1];
            player.password = coll[2];
            player.email = coll[3];
            return player;
        }
        else
        {
            return null;
        }
    }

    public static bool InsertPlayer(string user, string pass, string email)
    {
        try
        {
            Insert($"INSERT INTO Players(username, password, email) VALUES ('{user}', '{pass}','{email}')");
            return true;
        }
        catch
        {
            return false;
        }
    }
    /// <summary>
    /// Returns first occurence of data row
    /// <returns></returns>
    private static string[] Get(string query)
    {

        string[] result = { };
        lock (lockObject)
        {
            using SqliteConnection c = new SqliteConnection(connectionString);
            {
                c.Open();
                using SqliteCommand command = new SqliteCommand(query, c);
                {
                    using SqliteDataReader reader = command.ExecuteReader();
                    {
                        if (reader.Read())
                        {
                            result = new string[reader.FieldCount];
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                result[i] = reader[i].ToString();
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private static void Insert(string query)
    {
        lock (lockObject)
        {
            using SqliteConnection c = new SqliteConnection(connectionString);
            {
                c.Open();
                using SqliteCommand command = new SqliteCommand(query, c);
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
