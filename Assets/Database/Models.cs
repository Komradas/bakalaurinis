﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    public class Player
    {
        public int id;
        public string username;
        public string password;
        public string email;
    }

    public class Friends
    {
        public int fk_id_first;
        public int fk_id_second;
    }

    public class Match
    {
        public int[] playerIds;
        public int redTeamScore;
        public int blueTeamScore;
    }
}
