using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit;
using UnityEngine.SceneManagement;
using Multiplayer.Client;
using TMPro;
using Multiplayer.Server;

public class ServerIntegrationTestScript : MonoBehaviour
{

    [UnityTest]
    public IEnumerator TestMainServer()
    {
        SceneManager.LoadScene("Assets/Scenes/MasterServer.unity");
        yield return new MonoBehaviourTest<MainServerTests>();
        Application.Quit();
    }

    [UnityTest]
    public IEnumerator TestSessionServer()
    {
        SceneManager.LoadScene("Assets/Scenes/ServerScene.unity");
        yield return new MonoBehaviourTest<SessionServerTests>();
        Application.Quit();
    }

    public class MainServerTests : MonoBehaviour, IMonoBehaviourTest
    {
        public bool IsTestFinished
        {
            get { return TestCondition(); }
        }

        bool TestCondition()
        {
            if (MasterHandler.instance == null)
            {
                return false;
            }
            if (MasterHandler.instance.GetMasterServer().clients == null)
            {
                return false;
            }
            return MasterHandler.instance.GetMasterServer().matchmaker.players == 1;
        }
    }

    public class SessionServerTests : MonoBehaviour, IMonoBehaviourTest
    {
        Vector3 lastPos, currentPos = Vector3.zero;
        ServerBallController ball;


        public bool IsTestFinished
        {
            get { return TestCondition(); }
        }

        // trying to respawn the ball one frame it should be z = +- 50
        bool TestCondition()
        {
            return Vector3.Distance(currentPos, lastPos) > 30;
        }


        private void Update()
        {
            if (ball == null)
            {
                ball = (ServerBallController)FindObjectsOfType(typeof(ServerBallController))[0];
            }
            lastPos = currentPos;
            currentPos = ball.position;
        }
    }
}
