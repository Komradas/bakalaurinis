using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit;
using UnityEngine.SceneManagement;
using Multiplayer.Client;
using TMPro;

public class IntegrationTestScript : MonoBehaviour
{

    [UnityTest]
    public IEnumerator TestLogin()
    {
        SceneManager.LoadScene(Constants.LOGIN_SCENE);
        yield return new MonoBehaviourTest<LoginTestBehaviour>();
        Application.Quit();
    }

    [UnityTest]
    public IEnumerator TestRegister()
    {
        SceneManager.LoadScene(Constants.LOGIN_SCENE);
        yield return new MonoBehaviourTest<RegisterTestBehaviour>();
        Application.Quit();
    }

    [UnityTest]
    public IEnumerator TestGameScene()
    {
        SceneManager.LoadScene(Constants.LOGIN_SCENE);
        yield return new MonoBehaviourTest<GameSceneTests>();
        Application.Quit();
    }

    public class GameSceneTests : MonoBehaviour, IMonoBehaviourTest
    {
        bool sent = false;
        bool started = false;

        Vector3 startPos;
        Vector3 currentPos;

        public bool IsTestFinished { get; private set; }

        private void Update()
        {
            if (SceneManager.GetActiveScene().buildIndex != Constants.MENU_SCENE && !started)
            {
                return;
            }

            if (!started)
            {
                started = true;
                MasterHandler.instance.StartMatch("127.0.0.1", 26950, Team.BlueTeam);
            }
            if (ClientManager.instance == null)
            {
                return;
            }

            if (ClientManager.instance.client.isConnected && !sent)
            {
                sent = true;
                startPos = ClientManager.instance.GetPlayer(1).position;

            }
            currentPos = ClientManager.instance.GetPlayer(1).position;
            bool changedPosition = currentPos != startPos;
            IsTestFinished = ClientManager.instance.chatController.MessageCount() > 0 && changedPosition;
        }
    }



    public class LoginTestBehaviour : MonoBehaviour, IMonoBehaviourTest
    {
        bool sent = false;
        public bool IsTestFinished
        {
            get { return TestCondition(); }
        }

        bool TestCondition()
        {
            return SceneManager.GetActiveScene().buildIndex == Constants.MENU_SCENE;
        }

        private void Update()
        {
            if (!sent && MasterHandler.instance.connected)
            {
                sent = true;
                MasterClientSend.TryToLogin("TestAcc", "123456");
            }
        }
    }

    public class RegisterTestBehaviour : MonoBehaviour, IMonoBehaviourTest
    {
        bool sent = false;
        public bool IsTestFinished
        {
            get { return TestCondition(); }
        }

        bool TestCondition()
        {
            if (GameObject.Find("ErrorText") == null)
            {
                return false;
            }
            if (GameObject.Find("ErrorText").GetComponent<TextMeshProUGUI>() == null)
            {
                return false;
            }
            return GameObject.Find("ErrorText").GetComponent<TextMeshProUGUI>().text.Length > 0;
        }

        private void Update()
        {
            if (!sent && MasterHandler.instance.connected)
            {
                sent = true;
                MasterClientSend.CreateAccount("TestAcc", "123456", "");
            }
        }
    }
}
