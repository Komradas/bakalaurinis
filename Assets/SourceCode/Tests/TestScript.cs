using System.Collections;
using System.Collections.Generic;
using Multiplayer;
using Multiplayer.Client;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

public class UnitTests
{
    // A Test behaves as an ordinary method
    [Test]
    public void TestBallPhysics()
    {
        float x = BallPhysics.CalculateDistance(2);
        Vector3 velocity = BallPhysics.CalculateVelocity(Vector3.zero, Vector3.forward, 1f);
        bool equal = Vector3.Distance(new Vector3(0.0f, 4.4f, 1.1f), velocity) < 0.1f;
        Assert.IsTrue(equal);
    }
    [Test]
    public void TestDatabase()
    {
        Db.InsertPlayer("User", "123456", "Test@email.com");
        Database.Player pl = Db.GetPlayer("User");
        Assert.AreEqual("User", pl.username);

    }
    [Test]
    public void TestClientBallController()
    {
        GameObject ball = new GameObject();
        ClientBallController controller = ball.AddComponent<ClientBallController>();
        var temp = controller.position;
        controller.UpdatePosition(temp + Vector3.up);
        Assert.AreNotEqual(temp, controller.position);
    }

    [Test]
    public void PacketTest1()
    {
        using Packet packet = new Packet();
        byte b = 63;
        packet.Write(b);
        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadByte(), b);
    }
    [Test]
    public void PacketTest2()
    {
        using Packet packet = new Packet();
        bool bl = false;
        packet.Write(bl);
        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadBool(), bl);
    }
    [Test]
    public void PacketTest3()
    {
        using Packet packet = new Packet();
        short sh = 256;
        packet.Write(sh);
        packet.ToArray();
        Assert.AreEqual(packet.ReadShort(), sh);
    }
    [Test]
    public void PacketTest4()
    {
        using Packet packet = new Packet();
        int _int = 32;
        packet.Write(_int);
        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadInt(), _int);
    }
    [Test]
    public void PacketTest5()
    {
        using Packet packet = new Packet();
        float _float = 0.55f;
        packet.Write(_float);

        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadFloat(), _float);

    }
    [Test]
    public void PacketTest6()
    {
        using Packet packet = new Packet();
        string _str = "string";
        packet.Write(_str);
        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadString(), _str);
    }
    [Test]
    public void PacketTest7()
    {
        using Packet packet = new Packet();
        Vector3 vector = Vector3.one;
        packet.Write(vector);

        packet.SetBytes(packet.ToArray());
        Assert.AreEqual(packet.ReadVector3(), vector);
    }
    [Test]
    public void PacketTest8()
    {
        using Packet packet = new Packet();
        Vector2 vector2 = Vector2.one;
        packet.Write(vector2);

        packet.ToArray();
        Assert.AreEqual(packet.ReadVector2(), vector2);
    }
    [Test]
    public void PacketTest9()
    {
        using Packet packet = new Packet();
        Quaternion quaternion = Quaternion.identity;
        packet.Write(quaternion);
        packet.ToArray();
        Assert.AreEqual(packet.ReadQuaternion(), quaternion);
    }

}
