using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Numerics;
using UnityEngine;

namespace Multiplayer.Server
{
    public class Client
    {
        public ServerPlayerController player;
        public int id;
        public TCP tcp;
        public UDP udp;
        private IServer server;

        public Client(int _clientId, IServer _server, ServerPlayerController _player)
        {
            player = _player;
            id = _clientId;
            tcp = new TCP(id, _server);
            udp = new UDP(id, _server);
            server = _server;
        }

        public Client(int _clientId, IServer _server)
        {
            id = _clientId;
            tcp = new TCP(id, _server);
            server = _server;
        }


        public void Disconnect()
        {
            Debug.Log($"User {id} has disconnected.");
            if (player != null)
            {
                player.Disconnect();
            }
            if (tcp != null)
            {
                tcp.Disconnect();
            }
            if (udp != null)
            {
                udp.Disconnect();
            }
            if (server != null)
            {
                server.DisconnectUser(id);
            }
        }
    }
}
