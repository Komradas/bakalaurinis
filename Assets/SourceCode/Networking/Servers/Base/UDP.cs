﻿using System;
using System.Net;
using UnityEngine;

namespace Multiplayer.Server
{
    public class UDP
    {
        public IPEndPoint endPoint;

        private int id;
        IServer server;

        public UDP(int _id, IServer _server)
        {
            id = _id;
            server = _server;
        }

        public void Connect(IPEndPoint _endPoint)
        {
            endPoint = _endPoint;
        }

        public void SendData(Packet _packet)
        {
            try
            {
                if (endPoint != null)
                {
                    server.udpListener.BeginSend(_packet.ToArray(), _packet.Length(), endPoint, null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to {endPoint} via UDP: {_ex}");
            }
        }


        public void HandleData(Packet _packetData)
        {
            int _packetLength = _packetData.ReadInt();
            byte[] _packetBytes = _packetData.ReadBytes(_packetLength);

            ThreadManager.ExecuteOnMainThread(() =>
            {
                using (Packet _packet = new Packet(_packetBytes))
                {
                    server.HandleData(id, _packet);
                }
            });
        }

        public void Disconnect()
        {
            endPoint = null;
        }
    }
}
