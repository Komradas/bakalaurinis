﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Multiplayer.Server
{
    public abstract class IServer
    {
        public int port { get; protected set; }
        public Dictionary<int, Client> clients = new Dictionary<int, Client>();
        public delegate void PacketHandler(int _fromClient, Packet _packet);
        public Dictionary<int, PacketHandler> packetHandlers { get; set; }

        public TcpListener tcpListener { get; protected set; }
        public UdpClient udpListener { get; protected set; }

        public virtual void HandleData(int _clientId, Packet _packet)
        {
            int _packetId = _packet.ReadInt();
            packetHandlers[_packetId](_clientId, _packet);

        }

        public virtual void DisconnectUser(int id)
        {

        }

        public abstract void Start(int Port);
        public abstract void Stop();
    }
}
