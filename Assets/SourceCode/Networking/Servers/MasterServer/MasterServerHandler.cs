﻿using UnityEngine;

namespace Multiplayer.Server
{
    class MasterServerHandler
    {
        static MasterServer server;

        public static void SetServer(MasterServer _server)
        {
            server = _server;
        }

        public static void Login(int _fromClient, Packet _packet)
        {
            string _user = _packet.ReadString();
            string _pass = _packet.ReadString();
            bool _succes = false;

            var _player = Db.GetPlayer(_user);
            if (_player != null)
            {
                _succes = _player.password.Equals(_pass);
            }
            MasterServerSend.LoginResponse(_fromClient, _user, _succes);
        }

        public static void Register(int _fromClient, Packet _packet)
        {
            string _user = _packet.ReadString();
            string _pass = _packet.ReadString();
            string _email = _packet.ReadString();

            byte code;
            var _player = Db.GetPlayer(_user);
            if (_player != null)
            {
                code = 1;
            }
            else
            {
                code = 0;
                bool success = Db.InsertPlayer(_user, _pass, _email);
                if (!success)
                {
                    code = 2;
                }
            }
            MasterServerSend.RegisterResponse(_fromClient, code);
        }

        public static void CancelSearch(int _fromClient, Packet _packet)
        {
            Debug.Log($"Client: {_fromClient} has stoped searching for a game!");
            server.RemovePlayerFromMatchmaking(_fromClient);
        }

        public static void SearchForGame(int _fromClient, Packet _packet)
        {
            Debug.Log($"Client: {_fromClient} is searching for a game!");
            server.AddPlayerToMatchmaking(_fromClient);
        }

        public static void Message(int _fromClient, Packet _packet)
        {
            Debug.Log($"Client: {_fromClient} sent message!");
        }
    }
}
