﻿using static Multiplayer.Server.Matchmaker;

namespace Multiplayer.Server
{
    class MasterServerSend
    {
        private static IServer server;
        public static void SetServer(IServer _server)
        {
            server = _server;
        }

        public static void Welcome(int _clientId)
        {
            using Packet _packet = new Packet((int)MasterServerPackets.welcome);
            _packet.Write(_clientId);
            SendTCPData(_clientId, _packet);
        }

        public static void LoginResponse(int _clientId, string _name, bool _succes)
        {
            using Packet _packet = new Packet((int)MasterServerPackets.loginResponse);
            _packet.Write(_succes);
            _packet.Write(_name);           

            SendTCPData(_clientId, _packet);
        }

        public static void RegisterResponse(int _clientId, byte _code)
        {
            using Packet _packet = new Packet((int)MasterServerPackets.registerResponse);
            _packet.Write(_code);
            SendTCPData(_clientId, _packet);
        }

        public static void MatchFound(Player _player, string _ip, int _port)
        {
            using Packet _packet = new Packet((int)MasterServerPackets.matchFound);
            _packet.Write(_ip);
            _packet.Write(_port);
            _packet.Write((byte)_player.team);
            SendTCPData(_player.socketId, _packet);
        }

        #region TCP send
        private static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            server.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                server.clients[i].tcp.SendData(_packet);
            }
        }
        #endregion
    }
}
