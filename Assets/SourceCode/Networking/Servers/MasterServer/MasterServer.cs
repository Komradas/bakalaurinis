using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;


namespace Multiplayer.Server
{
    public class MasterServer : IServer
    {
        private int index = 0;
        public Matchmaker matchmaker { get; private set; }

        public MasterServer(int playerCount)
        {
            matchmaker = new Matchmaker(playerCount);
        }

        public void AddPlayerToMatchmaking(int id)
        {
            matchmaker.AddPlayer(id);
        }

        public void RemovePlayerFromMatchmaking(int id)
        {
            matchmaker.RemovePlayer(id);
        }

        public override void Start(int _port)
        {
            port = _port;
            Debug.Log("Starting server");

            InitializeServerData();

            tcpListener = new TcpListener(IPAddress.Any, port);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);

            Debug.Log($"Server started on port {port}.");
        }


        private void TCPConnectCallback(IAsyncResult _result)
        {
            TcpClient _tcpClient = tcpListener.EndAcceptTcpClient(_result);
            tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);
            Debug.Log($"Incoming connection ...");


            Client _client = new Client(index++, this);
            clients.Add(_client.id, _client);
            _client.tcp.Connect(_tcpClient, MasterServerSend.Welcome);

            Debug.Log($"Client {_client.id} connected");
        }

        public override void DisconnectUser(int id)
        {
            if (clients.ContainsKey(id))
            {
                clients.Remove(id);
            }
            matchmaker.RemovePlayer(id);

        }

        public override void Stop()
        {
            tcpListener.Stop();
        }

        private void InitializeServerData()
        {
            packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)MasterClientPackets.login, MasterServerHandler.Login },
                { (int)MasterClientPackets.createAccount, MasterServerHandler.Register },
                { (int)MasterClientPackets.message, MasterServerHandler.Message },
                { (int)MasterClientPackets.searchForGame, MasterServerHandler.SearchForGame },
                { (int)MasterClientPackets.cancelSearch, MasterServerHandler.CancelSearch },

            };
        }
    }
}
