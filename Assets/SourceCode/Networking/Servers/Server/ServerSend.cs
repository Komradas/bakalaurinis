﻿using System;
using UnityEngine;

namespace Multiplayer.Server
{
    class ServerSend
    {
        private static IServer server;

        public static void SetServer(IServer _server)
        {
            server = _server;
        }

        public static void Welcome(int _clientId)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.welcome);
            _packet.Write(_clientId);
            _packet.Write(ServerManager.currentTime);
            _packet.Write(ServerManager.instance.blueTeamScore);
            _packet.Write(ServerManager.instance.redTeamScore);

            SendTCPData(_clientId, _packet);
        }

        public static void SyncData(int _clientId)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.syncData);

            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                ServerPlayerController player = server.clients[i].player;
                _packet.Write(player.nickname);
                _packet.Write((byte)player.team);

            }

            SendTCPDataToAll(_packet);
        }

        public static void PlayerPosition(ServerPlayerController _player)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.playerPosition);
            _packet.Write(_player.id);
            _packet.Write(_player.position);
            _packet.Write(_player.animationDirection);
            _packet.Write(Time.time);

            SendUDPDataToAll(_packet);
        }

        public static void PlayerRotation(ServerPlayerController _player)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.playerRotation);
            _packet.Write(_player.id);
            _packet.Write(_player.rotation);

            SendUDPDataToAll(_player.id, _packet);
        }

        public static void BallTransform(ServerBallController _ball)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.ballTransform);
            _packet.Write(_ball.position);
            _packet.Write(_ball.rotation);
            SendUDPDataToAll(_packet);
        }

        public static void UpdateScore(int _redScore, int _blueScore)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.addScore);

            _packet.Write(ServerManager.currentTime);
            _packet.Write(_blueScore);
            _packet.Write(_redScore);

            SendTCPDataToAll(_packet);
        }

        public static void StartGame()
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.startGame);
            _packet.Write(ServerManager.currentTime);
            SendTCPDataToAll(_packet);

        }

        public static void EndGame(bool _forced)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.endGame);
            _packet.Write(_forced);
            _packet.Write(ServerManager.instance.redTeamScore);
            _packet.Write(ServerManager.instance.blueTeamScore);

            SendTCPDataToAll(_packet);
        }

        public static void PlayerMessage(int _id, string _message)
        {
            using Packet _packet = new Packet((int)Multiplayer.GameServerPackets.playerMessage);
            _packet.Write(_id);
            _packet.Write(_message);
            SendTCPDataToAll(_id, _packet);
        }

        #region TCP send
        private static void SendTCPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            server.clients[_toClient].tcp.SendData(_packet);
        }

        private static void SendTCPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                server.clients[i].tcp.SendData(_packet);
            }
        }

        private static void SendTCPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                if (i != _exceptClient)
                {
                    server.clients[i].tcp.SendData(_packet);
                }
            }
        }
        #endregion

        #region UDP send

        private static void SendUDPData(int _toClient, Packet _packet)
        {
            _packet.WriteLength();
            server.clients[_toClient].udp.SendData(_packet);
        }

        private static void SendUDPDataToAll(Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                server.clients[i].udp.SendData(_packet);
            }
        }

        private static void SendUDPDataToAll(int _exceptClient, Packet _packet)
        {
            _packet.WriteLength();
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                if (i != _exceptClient)
                {
                    server.clients[i].udp.SendData(_packet);
                }
            }
        }
        #endregion

    }
}
