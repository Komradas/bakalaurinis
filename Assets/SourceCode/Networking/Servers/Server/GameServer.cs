using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;


namespace Multiplayer.Server
{
    public class GameServer : IServer
    {
        int max_players = 2;

        public GameServer(int player_count = 2)
        {
            max_players = player_count;
        }

        public bool CheckIfAllConnected()
        {
            bool _allConnected = true;
            for (int i = 1; i <= clients.Count; i++)
            {
                if (max_players <= i)
                {
                    return _allConnected;
                }
                if (!clients[i].player.connected)
                {
                    _allConnected = false;
                }
            }
            return _allConnected;
        }

        public override void Start(int _port)
        {
            port = _port;
            Debug.Log("Starting server");

            InitializeServerData();

            tcpListener = new TcpListener(IPAddress.Any, port);
            tcpListener.Start();
            tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);

            udpListener = new UdpClient(port);
            udpListener.BeginReceive(UDPReceiveCallback, null);

            Debug.Log($"Server started on port {port}, player count: {max_players}.");
        }


        private void TCPConnectCallback(IAsyncResult _result)
        {
            TcpClient _client = tcpListener.EndAcceptTcpClient(_result);
            tcpListener.BeginAcceptTcpClient(TCPConnectCallback, null);
            Debug.Log($"Incoming connection ...");

            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                if (clients[i].tcp.socket == null)
                {
                    clients[i].tcp.Connect(_client, ServerSend.Welcome);
                    return;
                }
            }

            Debug.Log($"User failed to connect: Server full!");
        }

        private void UDPReceiveCallback(IAsyncResult _result)
        {
            try
            {
                IPEndPoint _clientEndPoint = new IPEndPoint(IPAddress.Any, 0);
                byte[] _data = udpListener.EndReceive(_result, ref _clientEndPoint);
                udpListener.BeginReceive(UDPReceiveCallback, null);

                if (_data.Length < 4)
                {
                    return;
                }

                using (Packet _packet = new Packet(_data))
                {
                    int _clientId = _packet.ReadInt();

                    if (_clientId == 0)
                    {
                        return;
                    }

                    if (clients[_clientId].udp.endPoint == null)
                    {
                        clients[_clientId].udp.Connect(_clientEndPoint);
                        return;
                    }

                    if (clients[_clientId].udp.endPoint.ToString() == _clientEndPoint.ToString())
                    {
                        // Ensures that the client is not being impersonated by another by sending a false clientID
                        clients[_clientId].udp.HandleData(_packet);
                    }
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error receiving UDP data: {_ex}");
            }
        }

        public override void Stop()
        {
            tcpListener.Stop();
            udpListener.Close();
        }

        private void InitializeServerData()
        {
            var players = ServerManager.instance.players;
            for (int i = 1; i <= players.Values.Count; i++)
            {
                clients.Add(i, new Client(i, this, players[i]));
            }

            ServerHandler handler = new ServerHandler(this);

            packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)GameClientPackets.welcomeReceived, handler.WelcomeReceived },
                { (int)GameClientPackets.playerMovement, handler.PlayerMovement },
                { (int)GameClientPackets.playerRotation, handler.PlayerRotation },
                { (int)GameClientPackets.playerJump, handler.PlayerJump },
                { (int)GameClientPackets.playerKickBall, handler.PlayerKickBall },
                { (int)GameClientPackets.playerStopBall, handler.PlayerStopBall },
                { (int)GameClientPackets.playerMessage, handler.PlayerMessage },
            };
        }
    }
}
