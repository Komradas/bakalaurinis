using UnityEngine;
using Multiplayer.Server;
using System;
using Multiplayer;

public class ServerPlayerController : MonoBehaviour
{
    public Vector3 position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public Quaternion rotation
    {
        get { return transform.rotation; }
        set { transform.rotation = value; }
    }

    public Vector3 direction
    {
        get { return _direction; }
        set { _direction = value.normalized; }
    }



    public string nickname { get; set; }


    public Vector2 animationDirection
    {
        get { return _animationDirection; }
    }

    public bool isRunning { get; set; }



    public bool touchesBall { get; set; }
    public bool connected { get; private set; }
    public int id;

    [SerializeField] private float runSpeed = 8;
    [SerializeField] private float jogSpeed = 4;
    [SerializeField] private float gravity = 10.0f;
    [SerializeField] private float maxVelocityChange = 10.0f;
    [SerializeField] private float jumpHeight = 2.0f;
    [SerializeField] private Collider outOfBounds;
    [SerializeField] private Rigidbody playerRigidbody;
    [SerializeField] private ServerBallController ballController;


    private Vector3 targetVelocity;
    private Vector2 _animationDirection;
    private Vector3[] lastPosition = { Vector3.zero, Vector3.zero };

    private Vector3 _direction = Vector3.zero;

    [SerializeField] private float speed = 10.0f;
    [SerializeField] private float distToGround = 0.1f;
    private bool jump = false;
    private bool grounded = false;
    private Vector3 resetPosition = Vector3.zero;

    public Team team = Team.undecided;

    private void Start()
    {
        nickname = "User " + id;
    }

    private void FixedUpdate()
    {
        if (connected)
        {
            grounded = IsGrounded();
            UpdateSpeed();
            MoveRigidBody();
            CalculateAnimation();
            SentToClients();
        }
        else
        {
            Vector3 pos = Vector3.down * 3;
            pos.x = id;
            position = pos;

        }

    }

    public void Init(string _username, Team _team)
    {
        nickname = _username;
        connected = true;
        team = _team;

        resetPosition = ServerManager.instance.GetResetPosition(GetResetPositionIndex(), team);
        position = resetPosition;
    }

    public void ResetPosition(Team _team = Team.undecided)
    {
        if (connected)
        {
            position = resetPosition;
        }
    }

    public void Disconnect()
    {
        connected = false;
        team = Team.undecided;
    }

    public void Jump()
    {
        if (grounded)
        {
            jump = true;
        }
    }

    public void StopBall()
    {
        if (touchesBall && ballController)
        {
            ballController.Stop();
        }
    }

    public void KickBall(Vector3 _targetPosition, KickType _type, float _height)
    {
        if (touchesBall && ballController)
        {
            ballController.Launch(_targetPosition, _type, _height);
        }
    }

    private void CalculateAnimation()
    {
        var temp = -transform.InverseTransformPoint(lastPosition[1]);
        _animationDirection.x = temp.x;
        _animationDirection.y = temp.z;
        _animationDirection.Normalize();
    }

    private void SentToClients()
    {
        ServerSend.PlayerPosition(this);
        ServerSend.PlayerRotation(this);
    }

    private void UpdateSpeed()
    {
        if (isRunning)
        {
            speed = runSpeed;
            maxVelocityChange = runSpeed;
        }
        else
        {
            speed = jogSpeed;
            maxVelocityChange = jogSpeed;
        }
    }

    private bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    private int GetResetPositionIndex()
    {
        int count = 0;
        for (int i = 1; i < ServerManager.instance.players.Count; i++)
        {
            if (ServerManager.instance.players[i].team == team)
            {
                count++;
            }
        }
        return count;
    }

    private void MoveRigidBody()
    {
        lastPosition[1] = lastPosition[0];
        lastPosition[0] = transform.position;
        if (grounded)
        {
            targetVelocity = _direction * speed;
            Vector3 _velocity = playerRigidbody.velocity;
            Vector3 _velocityChange = (targetVelocity - _velocity);
            _velocityChange.x = Mathf.Clamp(_velocityChange.x, -maxVelocityChange, maxVelocityChange);
            _velocityChange.z = Mathf.Clamp(_velocityChange.z, -maxVelocityChange, maxVelocityChange);
            _velocityChange.y = 0;

            playerRigidbody.AddForce(_velocityChange, ForceMode.VelocityChange);

            if (jump)
            {
                playerRigidbody.velocity = new Vector3(_velocity.x, CalculateJumpVerticalSpeed(), _velocity.z);
                jump = false;
            }
        }
        playerRigidbody.AddForce(new Vector3(0, -gravity * playerRigidbody.mass, 0));
    }

    private void OnTriggerExit(Collider other)
    {
        if (other == outOfBounds)
        {
            ResetPosition();
        }
    }

    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2 * jumpHeight * gravity);
    }
}
