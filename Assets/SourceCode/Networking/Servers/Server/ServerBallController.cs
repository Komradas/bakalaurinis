using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Multiplayer.Server
{
    public class ServerBallController : MonoBehaviour
    {
        [SerializeField] Collider redTeamGates, blueTeamGates, outOfBounds;
        [SerializeField] Rigidbody rbody;


        private float gravity = Physics.gravity.y;

        public Vector3 position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Quaternion rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        private void FixedUpdate()
        {
            ServerSend.BallTransform(this);
        }

        public void Stop()
        {
            rbody.angularVelocity = Vector3.zero;
            rbody.velocity = Vector3.zero;
        }

        public void Launch(Vector3 _target, KickType _type, float _multiplier)
        {
            if (_type == KickType.HighKick)
            {
                float _height = Mathf.Log(Vector3.Distance(position, _target)) * _multiplier;
                if (_height < 0.1)
                {
                    return;
                }
                rbody.AddForce(BallPhysics.CalculateVelocity(position, _target, _height), ForceMode.Impulse);
            }
            if (_type == KickType.GroundKick)
            {
                Vector3 _direction = _target - position;
                rbody.AddForce(_direction * _multiplier, ForceMode.Impulse);
            }

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                other.GetComponent<ServerPlayerController>().touchesBall = false;
            }

            if (other == outOfBounds)
            {
                ResetPosition();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Player")
            {
                other.GetComponent<ServerPlayerController>().touchesBall = true;
            }

            if (other == redTeamGates)
            {
                ServerManager.instance.UpdateScore(Team.RedTeam);
                ResetPosition(Team.BlueTeam);
            }

            if (other == blueTeamGates)
            {
                ServerManager.instance.UpdateScore(Team.BlueTeam);
                ResetPosition(Team.RedTeam);
            }
        }

        private void ResetPosition(Team team = Team.undecided)
        {
            rbody.angularVelocity = Vector3.zero;
            rbody.velocity = Vector3.zero;
            float side;
            switch (team)
            {
                case Team.BlueTeam:
                    side = 13;
                    break;
                case Team.RedTeam:
                    side = -13;
                    break;
                default:
                    side = 0;
                    break;
            }

            position = new Vector3(0, 0.5f, side);

        }
    }
}