﻿using System.Collections.Generic;
using UnityEngine;

namespace Multiplayer.Server
{
    enum GameState { WarmUp, Playing, Paused, Finished }


    class ServerManager : MonoBehaviour
    {
        [SerializeField] private float warmUpTime = 120f;
        [SerializeField] private float gameTime = 30f;

        public static ServerManager instance;

        public static float currentTime
        {
            get
            {
                return instance.timer;
            }
        }

        private float timer = 50;



        public Dictionary<int, ServerPlayerController> players = new Dictionary<int, ServerPlayerController>();
        [SerializeField] private List<ServerPlayerController> inspectorPlayer;
        [SerializeField] private List<Transform> redSpawnPlaces;
        [SerializeField] private List<Transform> blueSpawnPlaces;

        private GameState gameState = GameState.WarmUp;
        public int redTeamScore = 0, blueTeamScore = 0;

        private GameServer server;


        public Vector3 GetResetPosition(int id, Team _team)
        {
            if (_team == Team.BlueTeam)
            {
                return redSpawnPlaces[id].position;
            }
            if (_team == Team.RedTeam)
            {
                return blueSpawnPlaces[id].position;
            }
            return Vector3.down;

        }


        private void Start()
        {
            Application.targetFrameRate = 60;
            Convert();
            timer = warmUpTime;
            bool _parse = int.TryParse(GetArg("-port"), out int _port);
            if (!_parse)
            {
                _port = Constants.PORT;
            }

            _parse = int.TryParse(GetArg("-players"), out int _player_count);
            if (!_parse)
            {
                _player_count = Constants.MAX_PLAYERS;
            }

            _parse = int.TryParse(GetArg("-time"), out int _match_timer);
            if (!_parse)
            {
                gameTime = 600f;
            }
            else
            {
                gameTime = _match_timer;
            }
            Debug.Log($"Match time will be {gameTime} seconds");

            server = new GameServer(_player_count);
            ServerSend.SetServer(server);
            server.Start(_port);
        }

        void Update()
        {
            switch (gameState)
            {
                case GameState.WarmUp:
                    if (server.CheckIfAllConnected())
                    {
                        Debug.Log("All players connected, starting..");
                        StartGame();
                    }
                    else
                    {
                        UpdateTimer();
                        if (timer <= 0)
                        {
                            StartEnding(true);
                        }
                    }
                    break;
                case GameState.Playing:
                    UpdateTimer();
                    if (timer <= 0)
                    {
                        StartEnding(false);
                    }
                    break;
                case GameState.Paused:
                    break;
                case GameState.Finished:
                    UpdateTimer();
                    EndGame();
                    break;
            }
        }

        public void UpdateScore(Team _team)
        {
            if (_team == Team.RedTeam)
            {
                redTeamScore++;
            }

            if (_team == Team.BlueTeam)
            {
                blueTeamScore++;
            }
            if (gameState == GameState.Playing)
            {
                for (int i = 1; i < players.Count; i++)
                {
                    players[i].ResetPosition(_team);
                }
            }
            ServerSend.UpdateScore(redTeamScore, blueTeamScore);
            Debug.Log(timer);
        }

        private void UpdateTimer()
        {
            timer -= Time.deltaTime;
        }

        private void StartEnding(bool _forced)
        {
            ServerSend.EndGame(_forced);
            timer = 10;
            gameState = GameState.Finished;
        }

        private void EndGame()
        {
            if (timer <= 0)
            {
                server.Stop();
                Application.Quit();
            }
        }

        private void StartGame()
        {
            redTeamScore = 0;
            blueTeamScore = 0;
            timer = gameTime;
            ServerSend.StartGame();
            gameState = GameState.Playing;
        }


        private static string GetArg(string name)
        {
            var args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == name && args.Length > i + 1)
                {
                    return args[i + 1];
                }
            }
            return null;
        }

        private void OnApplicationQuit()
        {
            server.Stop();
        }


        //List -> Dictionary
        private void Convert()
        {
            for (int i = 0; i < inspectorPlayer.Count; i++)
            {
                players.Add(i + 1, inspectorPlayer[i]);
            }
        }

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Debug.Log("Instance already exists, destroying object!");
                Destroy(this);
            }
        }

    }
}
