﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Multiplayer.Server
{
    class ServerHandler
    {
        IServer server;

        public ServerHandler(IServer _server)
        {
            server = _server;
        }

        public void WelcomeReceived(int _fromClient, Packet _packet)
        {
            int _clientIdCheck = _packet.ReadInt();
            string _username = _packet.ReadString();
            Team _team = (Team)_packet.ReadByte();

            server.clients[_fromClient].player.Init(_username, _team);

            ServerSend.SyncData(_fromClient);
            Debug.Log($"{_username} connected sucessfully");

            if (_fromClient != _clientIdCheck)
            {
                Debug.Log($"Player \"{_username}\" (ID: {_fromClient}) has assumed the wrong client ID ({_clientIdCheck})!");
            }
        }

        public void PlayerMovement(int _fromClient, Packet _packet)
        {
            Vector3 _position = _packet.ReadVector3();
            bool _running = _packet.ReadBool();
            ServerPlayerController _player = server.clients[_fromClient].player;
            _player.direction = _position;
            _player.isRunning = _running;
        }

        public void PlayerRotation(int _fromClient, Packet _packet)
        {
            Quaternion _rotation = _packet.ReadQuaternion();
            server.clients[_fromClient].player.rotation = _rotation;
        }

        public void PlayerJump(int _fromClient, Packet _packet)
        {
            server.clients[_fromClient].player.Jump();
        }

        public void PlayerKickBall(int _fromClient, Packet _packet)
        {
            Vector3 _targetPosition = _packet.ReadVector3();
            KickType _type = (KickType)_packet.ReadByte();
            float _height = _packet.ReadFloat();
            server.clients[_fromClient].player.KickBall(_targetPosition, _type, _height);
        }

        public void PlayerStopBall(int _fromClient, Packet _packet)
        {
            server.clients[_fromClient].player.StopBall();
        }

        public void PlayerMessage(int _fromClient, Packet _packet)
        {
            string _message = _packet.ReadString();
            ServerSend.PlayerMessage(_fromClient, _message);
        }
    }
}
