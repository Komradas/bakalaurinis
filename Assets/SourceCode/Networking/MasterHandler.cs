﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Multiplayer.Server;
using Multiplayer.Client;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;

public class MasterHandler : MonoBehaviour
{
    public static MasterHandler instance { get; private set; }

    enum HandlerType { Server, Client }

    [SerializeField] HandlerType type = HandlerType.Server;

    Multiplayer.Server.MasterServer masterServer;
    MasterClient masterClient;
    public bool connected
    {
        get
        {
            switch (type)
            {
                case HandlerType.Server:
                    return masterServer.tcpListener.Server.Connected;
                case HandlerType.Client:
                    return masterClient.tcp.socket.Connected;
            }
            return false;
        }
    }
    public string ip { get; private set; }
    public int port { get; private set; }
    public string nickname { get; set; }
    public Team team { get; private set; }
    public Config config { get; private set; }
    public float matchTime { get; private set; }

    public void StartMatch(string _ip, int _port, Team _team)
    {
        ip = _ip;
        port = _port;
        team = _team;
        MainMenu.MatchFound();
        StartCoroutine(WaitAndStart());
    }

    public Multiplayer.Server.MasterServer GetMasterServer()
    {
        return masterServer;
    }

    IEnumerator WaitAndStart()
    {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(Constants.GAME_SCENE);
    }

    private static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs();
        for (int i = 0; i < args.Length; i++)
        {
            if (args[i] == name && args.Length > i + 1)
            {
                return args[i + 1];
            }
        }
        return null;
    }

    private void Awake()
    {
        if (File.Exists(Config.PATH))
        {
            string txt = File.ReadAllText(Config.PATH);
            config = JsonUtility.FromJson<Config>(txt);
        }
        else
        {
            config = new Config()
            {
                ip = Constants.MASTER_SERVER_IP,
                port = Constants.MASTER_SERVER_PORT
            };
        }
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
            Application.logMessageReceived += Application_logMessageReceived;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }

        bool _parse = int.TryParse(GetArg("-time"), out int _match_timer);
        if (!_parse)
        {
            matchTime = 600f;
        }
        else
        {
            matchTime = _match_timer;
        }
        Debug.Log($"Match time will be {matchTime} seconds");
    }

    private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
    {
        string path = Application.dataPath + "/log.log";
        File.AppendAllText(path, $"{type}: {condition}\n{stackTrace}\n\n\n");
    }

    private void Start()
    {
        switch (type)
        {
            case HandlerType.Server:
                bool res = int.TryParse(GetArg("-players"), out int playerCount);
                if (!res)
                {
                    playerCount = 1;
                }
                masterServer = new Multiplayer.Server.MasterServer(playerCount);
                MasterServerHandler.SetServer(masterServer);
                MasterServerSend.SetServer(masterServer);
                masterServer.Start(Constants.MASTER_SERVER_PORT);
                break;
            case HandlerType.Client:
                masterClient = new MasterClient();
                masterClient.ConnectToServer(config.ip, config.port);
                MasterClientHandle.SetClient(masterClient);
                MasterClientSend.SetClient(masterClient);

                break;
        }
    }

    private void OnApplicationQuit()
    {
        File.WriteAllText(Config.PATH, JsonUtility.ToJson(config));
        Application.logMessageReceived -= Application_logMessageReceived;
        switch (type)
        {
            case HandlerType.Server:
                masterServer.Stop();
                break;
            case HandlerType.Client:
                masterClient.Disconnect();
                break;
        }
    }

}

