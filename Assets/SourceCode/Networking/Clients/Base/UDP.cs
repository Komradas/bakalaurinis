﻿using System;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Multiplayer.Client
{
    public class UDP
    {
        public UdpClient socket;
        public IPEndPoint endPoint;
        public int id;
        IClient client;


        public UDP(string _ip, int _port, int _myID, IClient _client)
        {
            endPoint = new IPEndPoint(IPAddress.Parse(_ip), _port);
            id = _myID;

            client = _client;
        }

        public void Connect(int _localPort)
        {
            socket = new UdpClient(_localPort);

            socket.Connect(endPoint);
            socket.BeginReceive(ReceiveCallback, null);

            using (Packet _packet = new Packet())
            {
                SendData(_packet);
            }
        }

        public void SendData(Packet _packet)
        {
            try
            {
                _packet.InsertInt(id);
                if (socket != null)
                {
                    socket.BeginSend(_packet.ToArray(), _packet.Length(), null, null);
                }
            }
            catch (Exception _ex)
            {
                Debug.Log($"Error sending data to server via UDP: {_ex}");
            }
        }

        private void ReceiveCallback(IAsyncResult _result)
        {
            try
            {
                byte[] _data = socket.EndReceive(_result, ref endPoint);
                socket.BeginReceive(ReceiveCallback, null);

                if (_data.Length < 4)
                {
                    client.Disconnect();
                    return;
                }

                HandleData(_data);
            }
            catch
            {
                Disconnect();
            }
        }


        private void HandleData(byte[] _data)
        {
            using (Packet _packet = new Packet(_data))
            {
                int _packetLength = _packet.ReadInt();
                _data = _packet.ReadBytes(_packetLength);
            }

            ThreadManager.ExecuteOnMainThread(() =>
            {
                using (Packet _packet = new Packet(_data))
                {
                    int _packetId = _packet.ReadInt();
                    client.Handle(_packetId, _packet);
                }
            });
        }

        private void Disconnect()
        {
            client.Disconnect();

            endPoint = null;
            socket = null;
        }
    }
}
