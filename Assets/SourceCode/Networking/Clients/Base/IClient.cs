﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Multiplayer.Client
{
    public abstract class IClient
    {
        public int myId = 0;
        public TCP tcp;
        public UDP udp;

        public bool isConnected = false;

        protected delegate void PacketHandler(Packet _packet);
        protected Dictionary<int, PacketHandler> packetHandlers;

        public abstract void ConnectToServer(string ip, int port);
        public abstract void Handle(int _id, Packet _packet);
        public abstract void Disconnect();

    }
}
