using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Multiplayer.Client
{
    public class MasterClientSend
    {
        static MasterClient client;
        public static void SetClient(MasterClient _client)
        {
            client = _client;
        }


        public static void TryToLogin(string _username, string _pass)
        {
            using Packet _packet = new Packet((int)MasterClientPackets.login);
            _packet.Write(_username);
            _packet.Write(_pass);

            SendTCPData(_packet);
        }

        public static void CreateAccount(string _user, string _pass, string _email)
        {
            using Packet _packet = new Packet((int)MasterClientPackets.createAccount);
            _packet.Write(_user);
            _packet.Write(_pass);
            _packet.Write(_email);

            SendTCPData(_packet);
        }

        public static void SearchForMatch()
        {
            using Packet _packet = new Packet((int)MasterClientPackets.searchForGame);
            SendTCPData(_packet);
        }

        public static void CancelSearch()
        {
            using Packet _packet = new Packet((int)MasterClientPackets.cancelSearch);
            SendTCPData(_packet);
        }



        private static void SendTCPData(Packet _packet)
        {
            _packet.WriteLength();
            client.tcp.SendData(_packet);
        }
    }
}