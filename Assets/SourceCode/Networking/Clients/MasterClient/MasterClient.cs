using System.Collections.Generic;
using UnityEngine;

namespace Multiplayer.Client
{
    public class MasterClient : IClient
    {

        public override void ConnectToServer(string _ip, int _port)
        {
            tcp = new TCP(this);

            InitializeClientData();

            isConnected = true;
            tcp.Connect(_ip, _port);
        }

        public override void Handle(int id, Packet packet)
        {
            packetHandlers[id](packet);
        }

        private void InitializeClientData()
        {
            packetHandlers = new Dictionary<int, PacketHandler>()
            {
                { (int)MasterServerPackets.welcome, MasterClientHandle.Welcome },
                { (int)MasterServerPackets.loginResponse, MasterClientHandle.LoginResponse },
                { (int)MasterServerPackets.registerResponse, MasterClientHandle.RegisterResponse },
                { (int)MasterServerPackets.matchFound, MasterClientHandle.MatchFound },
                { (int)MasterServerPackets.message, MasterClientHandle.Message }
            };
            Debug.Log("Initialized packets.");
        }

        public override void Disconnect()
        {
            if (isConnected)
            {
                isConnected = false;
                tcp.socket.Close();
                Debug.Log("Disconnected from server.");
            }
        }
    }
}