using UnityEngine;
using UnityEngine.SceneManagement;

namespace Multiplayer.Client
{
    public class MasterClientHandle
    {
        static MasterClient client;
        public static void SetClient(MasterClient _client)
        {
            client = _client;
        }

        public static void Welcome(Packet _packet)
        {
            int _myId = _packet.ReadInt();
            client.myId = _myId;
        }

        public static void LoginResponse(Packet _packet)
        {
            bool _connected = _packet.ReadBool();
            string _name = _packet.ReadString();
            if (_connected)
            {
                MasterHandler.instance.nickname = _name;
                SceneManager.LoadScene(Constants.MENU_SCENE);
            }
            else
            {
                LoginRegister.ShowResponse(Color.red, "Username or password is incorrect!");
            }
        }

        public static void RegisterResponse(Packet _packet)
        {
            byte _code = _packet.ReadByte();
            if (_code == 0)
            {
                LoginRegister.ShowResponse(Color.green, "Account created");
            }
            if (_code == 1)
            {
                LoginRegister.ShowResponse(Color.red, "Account with that username already exists");
            }
            if (_code == 2)
            {
                LoginRegister.ShowResponse(Color.red, "Error account could not be created");
            }
        }

        public static void Message(Packet _packet)
        {
            Debug.Log("Message packet received");
        }

        public static void MatchFound(Packet _packet)
        {
            string _ip = _packet.ReadString();
            int _port = _packet.ReadInt();
            Team _team = (Team)_packet.ReadByte();

            MasterHandler.instance.StartMatch(_ip, _port, _team);
        }

    }
}