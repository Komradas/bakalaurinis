using Multiplayer.Client;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatController : MonoBehaviour
{
    [SerializeField] GameObject chat_text;
    [SerializeField] GameObject text_prefab;
    [SerializeField] TMP_InputField chat_input;
    [SerializeField] ScrollRect scrollView;
    bool isChatFocused;


    public void AddMessage(string _message)
    {
        GameObject text_object = Instantiate(text_prefab);
        text_object.transform.SetParent(chat_text.transform);
        text_object.GetComponent<TMPro.TextMeshProUGUI>().text = _message;
        text_object.transform.localScale = Vector3.one;
        scrollView.verticalNormalizedPosition = chat_text.transform.childCount - 1;
    }

    public int MessageCount()
    {
        return chat_text.transform.childCount;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !isChatFocused)
        {
            ClientManager.instance.StopCurrentPlayer();
            isChatFocused = true;
            chat_input.ActivateInputField();
        }
        else if (Input.GetKeyDown(KeyCode.Return) && isChatFocused)
        {
            ClientManager.instance.ResumeCurrentPlayer();
            string msg = chat_input.text;
            chat_input.text = "";
            if (msg.Length > 0)
            {
                string player = "";
                if (MasterHandler.instance.nickname != null)
                {
                    player = MasterHandler.instance.nickname;
                }
                AddMessage(player + ": " + msg);
                ClientSend.Message(msg);
            }

            isChatFocused = false;
            chat_input.DeactivateInputField();
        }
    }
}
