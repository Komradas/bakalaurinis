using System.Net;
using UnityEngine;

namespace Multiplayer.Client
{
    public class ClientHandle
    {
        static GameClient client;

        public static void SetClient(GameClient _client)
        {
            client = _client;
        }


        public static void Welcome(Packet _packet)
        {
            int _myId = _packet.ReadInt();
            float _timer = _packet.ReadFloat();
            int _blueScore = _packet.ReadInt();
            int _redScore = _packet.ReadInt();

            ClientManager.instance.UpdateScoreAndTime(_blueScore, _redScore, _timer);
            client.Id = _myId;

            ClientSend.WelcomeReceived();
            ClientManager.instance.SetCurrentPlayer(_myId);
            client.udp.Connect(((IPEndPoint)client.tcp.socket.Client.LocalEndPoint).Port);
        }

        public static void SyncData(Packet _packet)
        {
            for (int i = 1; i <= Constants.MAX_PLAYERS; i++)
            {
                ClientPlayerController player = ClientManager.instance.GetPlayer(i);
                player.nickName = _packet.ReadString();
                player.SetTeam((Team)_packet.ReadByte());
            }
        }

        public static void StartGame(Packet _packet)
        {
            float _time = _packet.ReadFloat();
            ClientManager.instance.UpdateScoreAndTime(0, 0, _time);
        }


        public static void PlayerPosition(Packet _packet)
        {
            int _id = _packet.ReadInt();
            Vector3 _position = _packet.ReadVector3();
            Vector2 _direction = _packet.ReadVector2();
            float _time = _packet.ReadFloat();
            ClientPlayerController _player = ClientManager.instance.GetPlayer(_id);
            if (_player == null)
            {
                return;
            }
            _player.UpdatePosition(_position, _time);
            _player.SetDirectionForAnimtion(_direction);
        }

        public static void PlayerRotation(Packet _packet)
        {
            int _id = _packet.ReadInt();
            Quaternion _rotation = _packet.ReadQuaternion();

            ClientPlayerController _player = ClientManager.instance.GetPlayer(_id);
            if (_player == null)
            {
                return;
            }

            _player.SetRotation(_rotation);

        }

        public static void BallTransform(Packet _packet)
        {
            Vector3 _position = _packet.ReadVector3();
            Quaternion _rotation = _packet.ReadQuaternion();

            var _ball = ClientManager.instance.ballController;
            if (_ball == null)
            {
                return;
            }
            _ball.UpdatePosition(_position);
            _ball.rotation = _rotation;
        }

        public static void UpdateScore(Packet _packet)
        {
            float _time = _packet.ReadFloat();
            int _blueScore = _packet.ReadInt();
            int _redScore = _packet.ReadInt();

            ClientManager.instance.UpdateScoreAndTime(_blueScore, _redScore, _time);
        }

        public static void PlayerDisconnected(Packet _packet)
        {
            int _id = _packet.ReadInt();


        }

        public static void PlayerMessage(Packet _packet)
        {
            int _fromPlayer = _packet.ReadInt();
            string _message = _packet.ReadString();

            string full_message = $"{ClientManager.instance.GetPlayer(_fromPlayer).nickName}: {_message}";
            ClientManager.instance.chatController.AddMessage(full_message);
        }


        public static void EndGame(Packet _packet)
        {
            bool _forced = _packet.ReadBool();
            int _red = _packet.ReadInt();
            int _blue = _packet.ReadInt();

            ClientManager.instance.client.hasStoped = true;
            ClientManager.instance.EndGame(_forced, _red, _blue);
        }

    }
}