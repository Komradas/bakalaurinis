using Multiplayer.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ClientManager : MonoBehaviour
{

    public static ClientManager instance;

    public GameClient client { get; private set; }

    public Dictionary<int, ClientPlayerController> players = new Dictionary<int, ClientPlayerController>();
    public string nickname;

    [SerializeField] ClientPlayerController blueTeamPlayer1;
    [SerializeField] ClientPlayerController blueTeamPlayer2;
    [SerializeField] ClientPlayerController blueTeamPlayer3;

    [SerializeField] ClientPlayerController redTeamPlayer1;
    [SerializeField] ClientPlayerController redTeamPlayer2;
    [SerializeField] ClientPlayerController redTeamPlayer3;

    public ClientBallController ballController;
    [SerializeField] public UIManager uiManager;
    public ChatController chatController;

    [SerializeField] Transform mainCamera;
    private int currentPlayer;
    private float gameTimer = 600;



    public void Start()
    {
        players.Add(1, blueTeamPlayer1);
        players.Add(2, blueTeamPlayer2);
        players.Add(3, blueTeamPlayer3);
        players.Add(4, redTeamPlayer1);
        players.Add(5, redTeamPlayer2);
        players.Add(6, redTeamPlayer3);

        client = new GameClient();
        ClientHandle.SetClient(client);
        ClientSend.SetClient(client);
        if (MasterHandler.instance == null)
        {
            client.ConnectToServer("127.0.0.1", 26950);
        }
        else
        {
            Debug.Log(MasterHandler.instance.ip + " " + MasterHandler.instance.port);
            client.ConnectToServer(MasterHandler.instance.ip, MasterHandler.instance.port);
        }
    }

    public void EndGame(bool _forced, int _red, int _blue)
    {
        Cursor.lockState = CursorLockMode.None;
        if (_forced)
        {
            uiManager.FadeOut(LoadLevel);
        }
        else
        {
            StartCoroutine(ShowScore(_red, _blue));
        }
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene(Constants.MENU_SCENE);
        Destroy(this);
    }

    public void UpdateScoreAndTime(int _blue, int _red, float _time)
    {
        uiManager.UpdateScore(_blue, _red);
        UpdateTimer(_time);
    }

    public void UpdateTimer(float _time)
    {
        gameTimer = _time;
        uiManager.UpdateTimer(_time);
    }

    public ClientPlayerController GetPlayer(int _id)
    {
        return players[_id];
    }

    public void StopCurrentPlayer()
    {
        if (currentPlayer == 0)
        {
            return;
        }
        players[currentPlayer].canMove = false;
    }

    public void ResumeCurrentPlayer()
    {
        if (currentPlayer == 0)
        {
            return;
        }
        players[currentPlayer].canMove = true;
    }

    public void SetCurrentPlayer(int _id)
    {
        currentPlayer = _id;
        var player = players[_id];
        player.SetCurrentPlayer();

        mainCamera.position = player.transform.position + Vector3.up;
        mainCamera.parent = player.transform;
    }

    private void CalculateTimer()
    {
        gameTimer -= Time.deltaTime;
        uiManager.UpdateTimer(gameTimer);
    }

    private IEnumerator ShowScore(int _red, int _blue)
    {
        uiManager.ShowFinalScore(_red, _blue);
        yield return new WaitForSeconds(3);
        uiManager.FadeOut(LoadLevel);
        client.Disconnect();
        SceneManager.LoadScene(Constants.MENU_SCENE);
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    void Update()
    {
        CalculateTimer();
        if (Input.GetButtonDown("Cancel"))
        {
            if (Cursor.lockState == CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }

    private void OnApplicationQuit()
    {
        client.Disconnect();
        Destroy(this);
    }
}

public enum Team { BlueTeam, RedTeam, undecided }
