using UnityEngine;
using TMPro;
using Multiplayer.Client;
using Multiplayer;

public class ClientPlayerController : MonoBehaviour
{
    [SerializeField] private Material redMaterial;
    [SerializeField] private Material blueMaterial;
    public bool canMove { get; set; } = true;

    public string nickName
    {
        get { return _nickname; }
        set
        {
            nicknameText.text = value;
            _nickname = value;
        }
    }

    public Vector3 position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    [SerializeField] private Renderer shirtRenderer;
    [SerializeField] private Transform playerCameraParent;
    [SerializeField] private float lookSpeed = 2.0f;
    [SerializeField] private float lookXLimit = 60.0f;

    [SerializeField] private Animator animator;
    [SerializeField] private TextMeshPro nicknameText;
    private AnimatorController animatorController;

    [Header("Targeting Settings")]
    [SerializeField] private Transform target;
    [SerializeField] float minDistance;
    [SerializeField] float maxDistance;

    private Vector2 rotation = Vector2.zero;
    private string _nickname;
    private float lastUpdate = 0;
    private bool current = false;
    private Team team;
    private KickType kickType;
    private float kickTimer = 0;

    void Start()
    {
        animatorController = new AnimatorController(animator);
        kickType = KickType.HighKick;
        ClientManager.instance.uiManager.ShowKickType(kickType);
    }


    private void Update()
    {
        if (current && canMove)
        {
            UpdateTargetDistance();
            UpdateRotation();
            UpdateJump();
            UpdateKickBall();
        }
    }

    public void SetTeam(Team _team)
    {
        team = _team;
        if (_team == Team.RedTeam)
        {
            nicknameText.color = Color.red;
            shirtRenderer.material = redMaterial;
        }
        if (_team == Team.BlueTeam)
        {
            nicknameText.color = Color.blue;
            shirtRenderer.material = blueMaterial;
        }

    }

    private void FixedUpdate()
    {
        if (ClientManager.instance.client.isConnected && current && canMove)
        {
            SendMovementInputToServer();
        }
    }

    public void SetCurrentPlayer()
    {
        current = true;
        nicknameText.gameObject.SetActive(false);
    }

    public void UpdatePosition(Vector3 _position, float _updateTime = -1)
    {
        if (lastUpdate < _updateTime)
        {
            lastUpdate = _updateTime;
            position = _position;
        }
        if (_updateTime < 0)
        {
            position = _position;
            lastUpdate = 0;
        }
    }

    public void SetDirectionForAnimtion(Vector2 _direction)
    {
        if (animatorController != null)
            animatorController.SetMovement(_direction);
    }

    public void SetRotation(Quaternion _rotation)
    {
        transform.rotation = _rotation;
    }

    private void UpdateTargetDistance()
    {
        Vector3 _pos = target.localPosition;
        _pos.z = BallPhysics.CalculateDistance(kickTimer) * 5;
        target.localPosition = _pos;
    }


    private void UpdateKickBall()
    {
        if (Input.GetButtonDown("Stance"))
        {
            if (kickType == KickType.HighKick)
            {
                kickType = KickType.GroundKick;
            }
            else
            {
                kickType = KickType.HighKick;
            }
            ClientManager.instance.uiManager.ShowKickType(kickType);
        }

        if (Input.GetButtonDown("Stop"))
        {
            ClientSend.PlayerStopBall();
        }

        ClientManager.instance.uiManager.UpdateKickSlider(BallPhysics.CalculateDistance(kickTimer));
        if (Input.GetButton("Fire1"))
        {
            kickTimer += Time.deltaTime;
            kickTimer = Mathf.Clamp(kickTimer, 0, 3f);
        }
        if (Input.GetButtonUp("Fire1"))
        {
            ClientSend.PlayerKickBall(target.position, kickType);
            kickTimer = 0;
        }
    }

    private void UpdateJump()
    {
        if (Input.GetButtonDown("Jump"))
        {
            ClientSend.PlayerJump();
        }
    }

    private void UpdateRotation()
    {
        rotation.y += Input.GetAxis("Mouse X") * lookSpeed;
        rotation.x += -Input.GetAxis("Mouse Y") * lookSpeed;
        rotation.x = Mathf.Clamp(rotation.x, -lookXLimit, lookXLimit);
        playerCameraParent.localRotation = Quaternion.Euler(rotation.x, 0, 0);
        transform.eulerAngles = new Vector2(0, rotation.y);
    }

    private void SendMovementInputToServer()
    {
        Vector2 _direction = new Vector2
        {
            x = Input.GetAxis("Horizontal"),
            y = Input.GetAxis("Vertical")
        };
        Vector3 _moveDirection = (transform.forward * _direction.y) + (transform.right * _direction.x);


        ClientSend.PlayerRotation(transform.rotation);
        ClientSend.PlayerMovement(_moveDirection, Input.GetButton("Running"));
    }
}
