using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Multiplayer.Client
{
    public class ClientBallController : MonoBehaviour
    {
        private float lastUpdate;
        public Vector3 position
        {
            get { return transform.position; }
            set { transform.position = value; }
        }

        public Quaternion rotation
        {
            get { return transform.rotation; }
            set { transform.rotation = value; }
        }

        public void UpdatePosition(Vector3 _position, float _updateTime = -1)
        {
            if (lastUpdate < _updateTime)
            {
                lastUpdate = _updateTime;
                position = _position;
            }
            if (_updateTime < 0)
            {
                position = _position;
                lastUpdate = 0;
            }
        }
    }
}