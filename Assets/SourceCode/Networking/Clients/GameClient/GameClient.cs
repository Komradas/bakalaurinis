using System;
using System.Collections.Generic;
using System.Net;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Multiplayer.Client
{
    public class GameClient : IClient
    {
        public Team team;
        public bool hasStoped = false;

        public int Id
        {
            get { return myId; }
            set
            {
                myId = value;
                if (udp != null)
                {
                    udp.id = value;
                }
            }
        }

        public override void ConnectToServer(string ip, int port)
        {
            tcp = new TCP(this);
            udp = new UDP(ip, port, myId, this);

            InitializeClientData();

            isConnected = true;
            tcp.Connect(ip, port);
            Cursor.lockState = CursorLockMode.Locked;
        }

        public override void Handle(int id, Packet packet)
        {
            if (hasStoped)
            {
                return;
            }
            packetHandlers[id](packet);
        }

        private void InitializeClientData()
        {
            packetHandlers = new Dictionary<int, PacketHandler>()
        {
            { (int)GameServerPackets.welcome, ClientHandle.Welcome },
            { (int)GameServerPackets.startGame, ClientHandle.StartGame },
            { (int)GameServerPackets.endGame, ClientHandle.EndGame },
            { (int)GameServerPackets.playerPosition, ClientHandle.PlayerPosition },
            { (int)GameServerPackets.playerRotation, ClientHandle.PlayerRotation },
            { (int)GameServerPackets.ballTransform, ClientHandle.BallTransform },
            { (int)GameServerPackets.addScore, ClientHandle.UpdateScore },
            { (int)GameServerPackets.syncData, ClientHandle.SyncData },
            { (int)GameServerPackets.playerDisconnected, ClientHandle.PlayerDisconnected },
            { (int)GameServerPackets.playerMessage, ClientHandle.PlayerMessage }
        };
            Debug.Log("Initialized packets.");
        }

        public override void Disconnect()
        {
            if (isConnected)
            {
                isConnected = false;
                tcp.socket.Close();
                udp.socket.Close();

                Debug.Log("Disconnected from server.");
            }
        }
    }
}