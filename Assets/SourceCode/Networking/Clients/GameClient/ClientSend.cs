using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Multiplayer.Client
{
    public class ClientSend
    {
        static GameClient client;
        public static void SetClient(GameClient _client)
        {
            client = _client;
        }

        public static void WelcomeReceived()
        {
            using Packet _packet = new Packet((int)GameClientPackets.welcomeReceived);
            _packet.Write(client.myId);
            if (MasterHandler.instance != null)
            {
                _packet.Write(MasterHandler.instance.nickname);
                _packet.Write((byte)MasterHandler.instance.team);
            }
            else
            {
                _packet.Write("User");
                _packet.Write((byte)Team.BlueTeam);
            }


            SendTCPData(_packet);
        }


        public static void PlayerKickBall(Vector3 _direction, KickType _type, float _height = 1f)
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerKickBall);
            _packet.Write(_direction);
            _packet.Write((byte)_type);
            _packet.Write(_height);
            SendTCPData(_packet);
        }

        public static void PlayerRotation(Quaternion _rotation)
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerRotation);
            _packet.Write(_rotation);
            SendUDPData(_packet);
        }

        public static void PlayerMovement(Vector3 _position, bool _isRunning)
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerMovement);
            _packet.Write(_position);
            _packet.Write(_isRunning);
            SendUDPData(_packet);
        }

        public static void PlayerJump()
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerJump);
            SendTCPData(_packet);
        }

        public static void PlayerStopBall()
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerStopBall);
            SendTCPData(_packet);
        }

        public static void Message(string _message)
        {
            using Packet _packet = new Packet((int)GameClientPackets.playerMessage);
            _packet.Write(_message);
            SendTCPData(_packet);
        }


        private static void SendUDPData(Packet _packet)
        {
            _packet.WriteLength();
            client.udp.SendData(_packet);
        }

        private static void SendTCPData(Packet _packet)
        {
            _packet.WriteLength();
            client.tcp.SendData(_packet);
        }
    }
}