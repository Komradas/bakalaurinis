﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Multiplayer
{
    /// <summary>
    /// Packet from Server to client
    /// </summary>
    public enum GameServerPackets
    {
        welcome = 1,
        playerPosition,
        playerRotation,
        playerDisconnected,
        addScore,
        ballTransform,
        syncData,
        startGame,
        endGame,
        playerMessage
    }

    /// <summary>
    /// Packet from client to server
    /// </summary>
    public enum GameClientPackets
    {
        welcomeReceived = 1,
        playerMovement,
        playerRotation,
        playerJump,
        playerKickBall,
        playerStopBall,
        playerMessage
    }

    public enum MasterServerPackets
    {
        welcome = 1,
        loginResponse,
        message,
        matchFound,
        registerResponse,
    }

    public enum MasterClientPackets
    {
        login = 1,
        createAccount,
        message,
        searchForGame,
        cancelSearch,
    }

    public enum KickType
    {
        GroundKick,
        HighKick
    }
}
