﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using UnityEngine;

namespace Multiplayer.Server
{
    public class Matchmaker
    {
        private string server_path = Application.dataPath + "/../../Server/Server.x86_64";
        Dictionary<int, Player> playersSearchingForGame;
        int playerCount;
        int port = 49152;

        public int players { get { return playersSearchingForGame.Count; } }

        public Matchmaker(int _playerCount = 3)
        {
            playersSearchingForGame = new Dictionary<int, Player>();
            playerCount = _playerCount * 2;
        }

        public void AddPlayer(int id)
        {
            playersSearchingForGame.Add(id, new Player()
            {
                socketId = id,
                partyId = -1

            });
            var players = CheckIfEnoughtForMatch();
            if (players != null)
            {
                StartMatch(players);
            }
            PrintCount();
        }

        private void StartMatch(List<Player> players)
        {
            UnityEngine.Debug.Log("Enough players for match. Starting ...");

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = server_path;
            startInfo.Arguments = " -port " + port + " -players " + players.Count + " -time " + MasterHandler.instance.matchTime;
            Process.Start(startInfo);

            for (int i = 0; i < playerCount; i++)
            {
                Player player = players[i];
                if (i < playerCount / 2)
                {
                    player.team = Team.BlueTeam;
                }
                else
                {
                    player.team = Team.RedTeam;
                }

                MasterServerSend.MatchFound(player, GetIPAddress(), port);
            }


            port++;
        }

        static string GetIPAddress()
        {
            string externalIpString = new WebClient().DownloadString("http://icanhazip.com").Replace("\\r\\n", "").Replace("\\n", "").Trim();
            var externalIp = IPAddress.Parse(externalIpString);

            return externalIp.ToString();
        }

        public void RemovePlayer(int id)
        {
            if (playersSearchingForGame.ContainsKey(id))
            {
                playersSearchingForGame.Remove(id);
                PrintCount();
            }
        }

        private void PrintCount()
        {
            UnityEngine.Debug.Log($"Currently {playersSearchingForGame.Count} players are searching for game");
        }


        private List<Player> CheckIfEnoughtForMatch()
        {
            if (playersSearchingForGame.Count < playerCount)
            {
                return null;
            }

            bool partyExists = false;
            foreach (var item in playersSearchingForGame.Keys)
            {
                if (playersSearchingForGame[item].partyId != -1)
                {
                    partyExists = true;
                    break;
                }
            }

            if (!partyExists)
            {
                int count = 0;
                List<Player> ret = new List<Player>();
                foreach (var item in playersSearchingForGame.Keys)
                {
                    ret.Add(playersSearchingForGame[item]);
                    count++;
                    if (playerCount <= count)
                    {
                        break;
                    }
                }

                foreach (var item in ret)
                {
                    RemovePlayer(item.socketId);
                }
                return ret;
            }
            return null;
        }

        public class Player
        {
            public int socketId;
            public int partyId;
            public Team team = Team.undecided;
        }

    }
}
