using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const int PORT = 26950;
    public const int BUFFER_LENGHT = 1024;

    public const int MAX_PLAYERS = 6;

    public const string MASTER_SERVER_IP = "127.0.0.1";

    public const int MASTER_SERVER_PORT = 25900;


    // scenes
    public const int LOGIN_SCENE = 0;
    public const int MENU_SCENE = 1;
    public const int GAME_SCENE = 2;


}
