using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Multiplayer.Client;

public class LoginRegister : MonoBehaviour
{

    private const string USER_PREF = "football_user";
    private const string PASS_PREF = "football_pass";

    [SerializeField] TMP_InputField login_username;
    [SerializeField] TMP_InputField login_password;
    [SerializeField] TMP_InputField register_username;
    [SerializeField] TMP_InputField register_password;
    [SerializeField] TMP_InputField register_repeat_password;

    [SerializeField] Button login_button;
    [SerializeField] Button create_account_button;
    [SerializeField] Button back_button;
    [SerializeField] Button register_button;

    [SerializeField] TextMeshProUGUI error_text;

    [SerializeField] GameObject registerPanel, loginPanel;
    [SerializeField] Toggle remember_toggle;


    static TextMeshProUGUI response_text;

    void Start()
    {
        response_text = error_text;
        login_password.inputType = TMP_InputField.InputType.Password;
        register_password.inputType = TMP_InputField.InputType.Password;
        register_repeat_password.inputType = TMP_InputField.InputType.Password;


        if (!string.IsNullOrEmpty(MasterHandler.instance.config.username) &&
        !string.IsNullOrEmpty(MasterHandler.instance.config.password))
        {
            login_username.text = MasterHandler.instance.config.username;
            login_password.text = MasterHandler.instance.config.password;
            remember_toggle.isOn = true;
        }
        else
        {
            remember_toggle.isOn = false;
        }
    }

    public void PressLogIn()
    {
        ClearErrorText();
        string _pass = login_password.text;
        string _user = login_username.text;

        if (_pass.Length < 6)
        {
            ShowError("Password lenght must be 6 or more");
            return;
        }
        MasterHandler.instance.nickname = _user;
        MasterClientSend.TryToLogin(_user, _pass);
        if (remember_toggle.isOn)
        {
            MasterHandler.instance.config.username = _user;
            MasterHandler.instance.config.password = _pass;

        }
        else
        {
            MasterHandler.instance.config.username = "";
            MasterHandler.instance.config.password = "";
        }
    }

    public void PressRegister()
    {
        ClearErrorText();
        loginPanel.SetActive(false);
        registerPanel.SetActive(true);
    }

    public void PressCreateAccount()
    {
        ClearErrorText();
        string _pass = register_password.text;
        string _pass2 = register_repeat_password.text;
        string _user = register_username.text;

        if (_pass.Length < 6)
        {
            ShowError("Password lenght must be 6 or more");
            return;
        }
        if (!_pass.Equals(_pass2))
        {
            ShowError("Passwords does not match");
            return;
        }
        MasterClientSend.CreateAccount(_user, _pass, "test@acc.c");

    }

    public static void ShowResponse(Color color, string text)
    {
        if (response_text != null)
        {
            response_text.text = text;
            response_text.color = color;
        }
    }


    public void PressBack()
    {
        ClearErrorText();
        loginPanel.SetActive(true);
        registerPanel.SetActive(false);
    }

    private void ClearErrorText()
    {
        error_text.text = "";
    }

    private void ShowError(string _message)
    {
        error_text.text += _message + "\n";
    }
}
