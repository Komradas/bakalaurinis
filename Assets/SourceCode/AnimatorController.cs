using UnityEngine;

public class AnimatorController
{

    private Animator animator;

    public AnimatorController(Animator _animator)
    {
        animator = _animator;
    }

    public void SetMovement(Vector2 _direction)
    {
        SetHorizontal(_direction.x);
        SetVertical(_direction.y);
    }

    public void SetJump()
    {
        animator.SetTrigger("Jump");
    }

    public void SetVertical(float _value)
    {
        animator.SetFloat("Vertical", _value);
    }
    public void SetHorizontal(float _value)
    {
        animator.SetFloat("Horizontal", _value);
    }

    public void SetShoot()
    {
        animator.SetTrigger("Shoot");
    }

    public void SetShootAnimation(int _value)
    {
        animator.SetInteger("ShootAnimation", _value);
    }


}
