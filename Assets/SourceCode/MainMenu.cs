using Multiplayer.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] Button search_match_button;
    [SerializeField] Button stop_searching_button;
    [SerializeField] GameObject load_image;
    [SerializeField] GameObject found_text;


    static Button search_button;
    static GameObject static_found_text;

    private void Start()
    {
        search_button = stop_searching_button;
        static_found_text = found_text;
    }

    public static void MatchFound()
    {
        search_button.gameObject.SetActive(false);
        static_found_text.SetActive(true);
    }

    public void SearchForGame()
    {
        MasterClientSend.SearchForMatch();
        search_match_button.gameObject.SetActive(false);
        stop_searching_button.gameObject.SetActive(true);
        load_image.SetActive(true);
    }

    public void CancelSearch()
    {
        load_image.SetActive(false);
        MasterClientSend.CancelSearch();
        search_match_button.gameObject.SetActive(true);
        stop_searching_button.gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
