﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[System.Serializable]
public class Config
{
    public static string PATH
    {
        get
        {
            return Application.dataPath + "/config.json";
        }
    }


    public string username;
    public string password;
    public string ip;
    public int port;
}

