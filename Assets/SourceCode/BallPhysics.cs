using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPhysics
{
    private static float gravity = Physics.gravity.y;

    public static Vector3 CalculateVelocity(Vector3 _startPos, Vector3 _targetPos, float _height)
    {
        Vector3 _displacement = _targetPos - _startPos;
        _displacement.y = 0;

        Vector3 _velocityY = Vector3.up * Mathf.Sqrt(-2 * gravity * _height);
        Vector3 _velocityXZ = _displacement / CalculateTime(_startPos, _targetPos, _height);

        return _velocityXZ + _velocityY;
    }

    public static float CalculateTime(Vector3 _startPos, Vector3 _targetPos, float _height)
    {
        return (Mathf.Sqrt(-2 * _height / gravity) + Mathf.Sqrt(2 * ((_targetPos.y - _startPos.y) - _height) / gravity));
    }

    public static float CalculateDistance(float x)
    {
        return -0.5f * x * x + 2 * x;
    }
}

