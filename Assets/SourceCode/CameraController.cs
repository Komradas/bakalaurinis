﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraController : MonoBehaviour
{
    [SerializeField] private List<Transform> objectsLookingAtCamera;


    private void Update()
    {
        UpdateLookAtObjects();
    }

    private void UpdateLookAtObjects()
    {
        foreach (var item in objectsLookingAtCamera)
        {
            item.rotation = transform.rotation;
        }
    }



}
