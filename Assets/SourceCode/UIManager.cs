using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Multiplayer;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    TMPro.TMP_Text blueTeamScoreText,
        redTeamScoreText,
        timerText;

    [SerializeField] GameObject endPanel;
    [SerializeField] Image fadeOutImage;
    [SerializeField] TMPro.TMP_Text end_score;
    [SerializeField] GameObject ground_kick, high_kick;
    [SerializeField] Slider kick_slider;
    public delegate void Callback();

    public void UpdateTimer(float _value)
    {
        int _min = (int)(_value / 60f);
        int _secs = (int)(_value % 60f);
        if (_value < 0)
        {
            _min = 0;
            _secs = 0;
        }

        timerText.text = $"{_min} : {_secs.ToString().PadLeft(2, '0')}";
    }

    public void UpdateKickSlider(float _kick)
    {
        kick_slider.value = _kick;
    }

    public void UpdateScore(int _scoreA, int _scoreB)
    {
        blueTeamScoreText.text = "" + _scoreA;
        redTeamScoreText.text = "" + _scoreB;
    }

    public void FadeOut(Callback callback)
    {
        StartCoroutine(Fade(callback));
    }

    IEnumerator Fade(Callback callback)
    {
        Color color = Color.black;
        for (float ft = 0f; ft <= 1; ft += 0.05f)
        {
            color.a = ft;
            fadeOutImage.color = color;
            yield return null;
        }
        callback.Invoke();
    }

    public void ShowKickType(KickType _type)
    {
        if (_type == KickType.GroundKick)
        {
            ground_kick.SetActive(true);
            high_kick.SetActive(false);
        }
        if (_type == KickType.HighKick)
        {
            ground_kick.SetActive(false);
            high_kick.SetActive(true);
        }
    }

    public void ShowFinalScore(int _red, int _blue)
    {
        endPanel.gameObject.SetActive(true);

        if (_red < _blue)
        {
            end_score.text = $"BLUE TEAM WINS\n{_red} : {_blue}";
            end_score.color = Color.blue;
        }
        else if (_blue < _red)
        {
            end_score.text = $"RED TEAM WINS\n{_red} : {_blue}";
            end_score.color = Color.red;
        }
        else
        {
            end_score.text = $"TIE\n{_red} : {_blue}";
            end_score.color = Color.black;
        }


    }
}



